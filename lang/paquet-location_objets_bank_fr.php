<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/location_objets_bank.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'location_objets_bank_description' => 'Intègre les système de paiement de bank dans Location d’objets.',
	'location_objets_bank_nom' => 'Location d’objets - paiements',
	'location_objets_bank_slogan' => 'Faites payer vos locations'
);
